﻿using UnityEngine;
using System.Collections;





public class Minion : Unit {

  private Animator myAnimator;
  private NavMeshAgent myAgent;
  private Vector3 nextPosition;
  private bool finalBattle;

  protected override void Awake() {
    base.Awake();
    myAnimator = GetComponent<Animator>();
    myAgent = GetComponent<NavMeshAgent>();
  }

  // Update is called once per frame
  protected override void Update() {
    //attack => detect objective
    //movement

    base.Update();

    move();

  }

  protected override void OnEnable() {
    base.OnEnable();
    myState = State.Move;
    finalBattle = false;
  }

  protected override void OnDisable() {
    EventManager.OnPositionFree(new freePositionMsg(nextPosition));
    base.OnDisable();
  }

  public override void setStats(Stats new_stats) {
    myStats = new_stats;
    current_health = new_stats.maxHealth;
    myAgent.speed = myStats.speed;
  }

  public void getFinalPosition() {
    nextPosition = GameObject.Find("Floor").GetComponent<NavManager>().getNextPosition(myTransform.position, mySide);
  }

  public override float takeDamage(float value) {
    base.takeDamage(value);
    if (current_health <= 0) {
      gameObject.transform.parent = GameInstance.Instance.gameObject.transform;
      this.gameObject.SetActive(false);
    }
    return current_health;
  }

  public override void attack() {
    // animator
    if (myState == State.Attack) {
      myAgent.Stop();
      if (aux_timer >= myStats.attackSpeed) {
        myAnimator.SetBool("attack", true);
        myAnimator.SetBool("move", false);
      } else {
        myAnimator.SetBool("attack", false);
      }
    }
    base.attack();
    if (objectiveToAttack == null) { //killed
      myState = State.Move;
      //if the animator is changed here, the animation will be cancelled too early
      //myAnimator.SetBool("attack", false);
      //myAnimator.SetBool("move", true);
    }
  }

  public void move() {
    Vector3 reference;

    if (objectiveToAttack != null && !objectiveToAttack.name.Contains("Castle")) {
      reference = objectiveToAttack.position;
    } else {
      reference = nextPosition;
    }
    /*Smooth rotation*/
    //find the vector pointing from our position to the target
    Vector3 direction = (reference - myTransform.position);
    direction.y = 0.0f;
    //create the rotation we need to be in to look at the target
    Quaternion lookRotation = Quaternion.LookRotation(direction);
    //myTransform.rotation = lookRotation;//raw rotation
    myTransform.rotation = Quaternion.Slerp(myTransform.rotation,
                                          lookRotation,
                                          Time.deltaTime * 15.0f);//acelerated rotation

    if (myState == State.Move) {
      if (myAnimator.isInitialized) {//error avoidance
        myAnimator.SetBool("move", true);
        myAnimator.SetBool("attack", false);
      }
 

      if (!finalBattle && (reference - myAgent.nextPosition).magnitude < 2.0f) {//check distance
        Vector3 aux = EventManager.OnLockPosition(reference, mySide);
        if (reference == aux) {//locked my position
          finalBattle = true;//fighting with the castle => no more movements
          myState = State.IDLE;
          myAnimator.SetBool("move", false);
        } else {//new position
          if (aux.y == 0.0f) {
            //patrolling
            aux.y = -1.0f;
          }
          
          reference = aux;
        }
        nextPosition = aux;
      }
      if (myAgent.isOnNavMesh) { //error avoidance
        myAgent.destination = reference;
        myAgent.Resume();
      }
      
    }
  }


}
