﻿using UnityEngine;
using System.Collections;

public class Turret : Unit {

  public TurretSO turretStats;
  // Use this for initialization
  void Start () {
    myStats = getStats(turretStats);
    current_health = myStats.maxHealth;
  }

  public override void attack() {
    base.attack();
    if (objectiveToAttack == null) { //killed
      myState = State.IDLE;
    }

  }


  public override float takeDamage(float value) {
    base.takeDamage(value);
    if (current_health <= 0) {
      this.gameObject.SetActive(false);
    }
    return current_health;
  }

  Stats getStats(TurretSO turret) {
    Stats aux_stats = new Stats();
    if (GameInstance.Instance.PlayerTeam == 0){
      aux_stats.name = "Human "+turret.turretName;
    } else {
      aux_stats.name = "Skeleton " + turret.turretName;
    }
    aux_stats.maxHealth = turret.hp;
    aux_stats.attack = turret.attack;
    aux_stats.attackSpeed = turret.attackSpeed;
    aux_stats.range = turret.attackRange;
    aux_stats.detectionRange = turret.detectionRange;
    aux_stats.speed = 0;//no needed
    return aux_stats;
  }

}

