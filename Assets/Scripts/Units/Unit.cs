﻿using UnityEngine;
using System.Collections;

public struct Stats
{
    public string name;
    public float maxHealth;
    public float attack;
    public float attackSpeed;
    public float range;
    public float detectionRange;
    public float speed;
};
public enum Side {
  Left = 0,//attack
  Right   //deffense
};

public enum State {
  IDLE = 0,
  Move,
  Charge,
  Attack
};

public class Unit : MonoBehaviour {

  public RectTransform healthBar;

  protected Transform myTransform;
  protected Transform mainObjective;
  protected Transform objectiveToAttack;
  protected Collider myCollider;
  protected Stats myStats;
  protected float current_health;
  protected float aux_timer;
  public Side mySide;
  protected State myState;

  // Use this for initialization
  protected virtual void Awake() {
    myTransform = GetComponent<Transform>();
    myStats = new Stats();
    myCollider = myTransform.GetChild(0).GetComponent<Collider>();
    objectiveToAttack = null;
    aux_timer = 0.0f;
    myState = State.IDLE;
  }

  // Update is called once per frame
  protected virtual void Update() {

    if (objectiveToAttack != null && inAttackRange()){
      attack();
    }
    setTarget();
    updateHealthBar();

  }

  protected virtual void OnEnable() {
    objectiveToAttack = null;
    aux_timer = 0.0f;
    myState = State.IDLE;
    healthBar.GetChild(1).transform.localScale = new Vector3(1, 1, 1);

    //look for main objective in the game
    if (mySide == Side.Left) {
      mainObjective = GameObject.Find("Right_Castle").GetComponent<Transform>();
    } else {
      mainObjective = GameObject.Find("Left_Castle").GetComponent<Transform>();
    }

    EventManager.orderEvent += killMe;
  }

  protected virtual void OnDisable() {
    healthBar.gameObject.SetActive(false);
    EventManager.orderEvent -= killMe;
  }

  public virtual void setStats(Stats new_stats) {
    myStats = new_stats;
    current_health = new_stats.maxHealth;
  }

  public Side team {
    get { return mySide; }
    set { mySide = value; }
  }

  public string unitName {
    get { return myStats.name; }
    set { myStats.name = value; }
  }

  public virtual float takeDamage(float value) {
    current_health -= value;
    return current_health;
  }

  public float Health {
    get { return current_health; }
  }

  /// Checks if the minion can attack the objective detected
  protected virtual bool inAttackRange() {
    if (myState == State.Attack) return true;//no need to do calculations
    float distance = (objectiveToAttack.position - myTransform.position).magnitude;

    //Adjustment for animations
    if (objectiveToAttack.name.Contains("Castle")){
      distance -= 3.25f;
    } else {
      distance -= 2.0f;
    }

    if (distance <= myStats.range) {//in range
      aux_timer = myStats.attackSpeed;//stats attacking, no wait
      myState = State.Attack;
      return true;
    }
    return false;
  }

  public virtual void attack() {
    float health = 0;
    if (myState == State.Attack) {
      if (aux_timer >= myStats.attackSpeed) {//waits the corresponding time
        aux_timer = 0.0f;
        //Checks if is a minion or a turret
        if (objectiveToAttack.transform.parent) {
          Minion aux = objectiveToAttack.transform.parent.GetComponent<Minion>();
          if (aux.Health <= 0.0f) {//its already dead
            objectiveToAttack = null;
            return;//force exit
          } else {
            health = aux.takeDamage(myStats.attack);
          }
        } else {
          Turret aux = objectiveToAttack.transform.GetComponent<Turret>();
          if (aux.Health <= 0.0f){//its already dead
            objectiveToAttack = null;
            return;//force exit
          } else {
            health = aux.takeDamage(myStats.attack);
          }
          
        }
        if (health <= 0.0f){//killed
          sendMsg();//send a message to the gamemode
          objectiveToAttack = null;
        }
      } else {
        aux_timer += Time.deltaTime;
      }
    }
  }

  /// looks for targets in its range of detection
  protected virtual void setTarget() {

    //if already have an objective check if is still alive
    if (objectiveToAttack != null && !objectiveToAttack.gameObject.activeInHierarchy) {
      objectiveToAttack = null;
      myState = State.Move;
    }

    //not in range => enemy escape
    if (objectiveToAttack != null &&
        (objectiveToAttack.position - myTransform.position).magnitude > myStats.detectionRange) {
      objectiveToAttack = null;
    }

    //if my actual target is not a minion => recheck
    if (objectiveToAttack == mainObjective || objectiveToAttack == null) {
      Collider[] hitColliders = Physics.OverlapSphere(myTransform.position, myStats.detectionRange, 0x00000300); //mask = minion & building => avoid floor collider
      if (hitColliders.Length > 1) {//discard when only detects myself
        float minDistance = float.MaxValue;
        foreach (Collider col in hitColliders) {
          if (col.transform.name.Contains("Castle")) { //if is the castle turret
            //if is the only enemy detected
            if (hitColliders.Length == 2 &&
                isEnemy(transform.tag, col.transform.tag)) {
              objectiveToAttack = col.gameObject.transform;
              break;
            }
            continue;
          }
          //if is an enemy
          if (isEnemy(transform.tag, col.transform.parent.tag)) {
            float distance = (col.gameObject.transform.position - myTransform.position).magnitude;
            if (col != myCollider && minDistance > distance) {
              minDistance = distance;
              objectiveToAttack = col.gameObject.transform;
            }
          }
        }
      }
    }
    if (objectiveToAttack == null) {//if nothing detected return to main objective
      objectiveToAttack = mainObjective;
    }
  }

  protected bool isEnemy(string tag1, string tag2) {
    return tag1 != tag2;
  }

  protected virtual void updateHealthBar() {
    if (current_health < myStats.maxHealth) healthBar.gameObject.SetActive(true);
    if (healthBar.gameObject.activeSelf) {
      healthBar.LookAt(myTransform.position + Camera.main.transform.rotation * Vector3.back,
                       Camera.main.transform.rotation * Vector3.up);// healthbar like a billboard
      float value = Mathf.Clamp((current_health / myStats.maxHealth), 0, 1);
      healthBar.GetChild(1).transform.localScale = new Vector3(value, 1, 1);
      
    }
  }

  protected void sendMsg(){
    attackMsg data = new attackMsg();
    data.attacker = this.gameObject;
    if(objectiveToAttack.parent == null) data.killed = objectiveToAttack.gameObject;//turret
    else data.killed = objectiveToAttack.parent.gameObject;//minion
    EventManager.OnEventTriggered(data);
  }

  void killMe() {
    this.gameObject.SetActive(false);
  }

}
