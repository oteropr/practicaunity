﻿using UnityEngine;
using System.Collections;

public enum gameMode {
  None = 0,
  Single,
  Multi,
};

public class GameMode : MonoBehaviour {

  public float startTime=60 * 2;
  public float spawn_timer;

  private UIManager myUIManager;
  private MinionManager mySpawner;
  private float gameTimer, aux_timer;
  private int unitTypeSelected;
  private bool gameFinished;


  public float Timer {
    get { return gameTimer; }
    set { gameTimer = value; }
  }

  void OnDestroy(){
    EventManager.EventTriggered -= unitKilled;
  }

  // Use this for initialization
  void Start () {
    GameInstance.Instance.createPool();
    myUIManager = GetComponent<UIManager>();
    mySpawner = GameInstance.Instance.gameObject.GetComponent<MinionManager>();

    gameTimer = startTime;// secs
    aux_timer = 0.0f;
    unitTypeSelected = 0;
    gameFinished = false;
    EventManager.EventTriggered += unitKilled;
  }
	
  // Update is called once per frame
  void Update () {
    if (!gameFinished) {
      updateTimer();

      //control the cpu spawn
      if (aux_timer >= spawn_timer || Input.GetButtonDown("Fire2")) {//fire2 = debug
        aux_timer = 0.0f;
        mySpawner.spawnMinion(new int[] { 1, Random.Range(0, 4)},
                              (GameInstance.Instance.PlayerSide == 0) ? Side.Right : Side.Left);
      }

      //player spawn
      if (Input.GetButtonDown("Fire1")) {
        mySpawner.spawnMinion(new int[] { 0, unitTypeSelected }, GameInstance.Instance.PlayerSide);
      }


      //DEBUG - Pause the game
      if (Input.GetKeyDown(KeyCode.Space))Time.timeScale = 0;
      if (Input.GetKeyDown(KeyCode.Space) && Input.GetKey(KeyCode.LeftControl)) Time.timeScale = 1;

    }
  }

  public int UnitTypeSelected {
    get { return unitTypeSelected; }
    set { unitTypeSelected = value; }
  }

  private void updateTimer() {
    if (gameTimer <= 0.0f){
      gameFinished = true;
      myUIManager.setEndGameMsg("DRAW");
    } else {
      gameTimer -= Time.deltaTime;
      aux_timer += Time.deltaTime;
    }
  }

  //Received an event that a minon kill other
  public void unitKilled(attackMsg data){

    myUIManager.addLogMsg(data.attacker.GetComponent<Unit>().unitName +
                          " has killed " +
                          data.killed.GetComponent<Unit>().unitName);

    if (data.killed.name.Contains("Castle")) {
      gameFinished = true;
      EventManager.OnorderEvent();
      if (data.killed.name.Contains("Left")){//attack castle was destroyed
        myUIManager.setEndGameMsg("DEFFENSE\nWINS");
      } else {//deffense castle was destroyed
        myUIManager.setEndGameMsg("ATTACK\nWINS");
      }
    }
  }

}
