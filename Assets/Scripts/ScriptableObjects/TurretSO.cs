﻿using UnityEngine;
using System.Collections;
using System;

[CreateAssetMenu(fileName = "Turret",menuName = "Units/Turret", order = 3)]
[System.Serializable]
public class TurretSO : ScriptableObject {
    public string turretName;
    public float detectionRange, hp, attack, attackRange, attackSpeed;
    public GameObject model;
}
