﻿using UnityEngine;
using System.Collections;
using System;


public enum MinionType {
  Melee = 0,
  Range
};

[CreateAssetMenu(fileName = "Minion", menuName = "Units/Minion", order = 2)]

[System.Serializable]
public class MinionSO : ScriptableObject{

  public MinionType type;
  public string minionName;
  public float maxHealth;
  public float attack;
  public float attackSpeed;
  public float range;
  public float detectionRange;
  public float speed;

  public GameObject model;


  void OnValidate() {
    if(type == MinionType.Melee) {
      range = 1.0f;
    }
  }

}
