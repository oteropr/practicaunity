﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


[CreateAssetMenu(fileName = "Team", menuName = "Team", order = 1)]
[System.Serializable]
public class TeamSO : ScriptableObject{

  public string teamName;
  public List<MinionSO> characterList;
}
