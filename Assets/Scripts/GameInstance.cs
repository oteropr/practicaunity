﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public enum Scene{
  Main_Menu,
  Game
};


public class GameInstance : MonoBehaviour {

  private static GameInstance instance = null;
  private gameMode currentGameMode;//single or multi
  private Side playerSide;
  private Scene currentScene;
  private int playerTeam, enemyTeam;

  void Awake () {
    if (instance == null){
      instance = this;
    } else if (instance != this) {
      Destroy(gameObject);
    }
    DontDestroyOnLoad(gameObject);
	
  }

  void Start() {
    currentGameMode = gameMode.None;
    playerSide = Side.Left;
    currentScene = Scene.Main_Menu;
    playerTeam = 0;
    enemyTeam = 1;
  }
  
  //lazy singleton
  public static GameInstance Instance {
    get { return instance ?? (instance = GameObject.Find("Singleton").GetComponent<GameInstance>()); }

  }

  public gameMode GameMode {
    get { return currentGameMode; }
    set { currentGameMode = value; }
  }

  public Side PlayerSide {
    get { return playerSide; }
    set { playerSide = value; }
  }

  public Scene Scene {
    get { return currentScene; }
    set { currentScene = value; }
  }

  public int PlayerTeam {
    get { return playerTeam; }
    set { playerTeam = value; }
  }

  public int EnemyTeam {
    get { return enemyTeam; }
    set { enemyTeam = value; }
  }

  public void loadGameScene(){

    currentScene = Scene.Game;
    SceneManager.LoadScene("Game");

  }

  public void createPool() {
    gameObject.GetComponent<MinionManager>().createPool();
  }
}
