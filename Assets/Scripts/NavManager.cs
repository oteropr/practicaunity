﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PositionData {
  public bool avaliable;
  public Vector3 position;

  public PositionData(bool state, Vector3 new_position) {//Ctor
    this.avaliable = state;
    this.position = new_position;
  }

}




public class NavManager : MonoBehaviour {

  //public GameObject test;

  public Transform[] pivotsLeftCastle, pivotsRightCastle;
  public int attackPositions = 3;

  private Transform leftCastle, rightCastle;
  private PositionData[] leftCastlePositions, rightCastlePositions;
  private int lockedPositions;


  void OnDestroy() {
    EventManager.freePosition -= freePosition;
    EventManager.lockPosition -= getNextPosition;
  }

  // Use this for initialization
  void Start() {
    float angle = 0.0f;
    leftCastle = GameObject.Find("Left_Castle").GetComponent<Transform>();
    rightCastle = GameObject.Find("Right_Castle").GetComponent<Transform>();

    leftCastlePositions = new PositionData[attackPositions];
    rightCastlePositions = new PositionData[attackPositions];

    //prepare the positions arund the tower
    for (int i = 0; i < attackPositions; ++i) {
      angle = i * 2 * Mathf.PI / attackPositions;
      float radio = 2f;
      Vector3 new_position = new Vector3(leftCastle.position.x + radio * Mathf.Cos(angle),
                                         -1.0f,
                                         leftCastle.position.z + radio * Mathf.Sin(angle));
      leftCastlePositions[i]=new PositionData(true, new_position);

      new_position = new Vector3(rightCastle.position.x + radio * Mathf.Cos(Mathf.PI - angle),
                                 -1.0f,
                                 rightCastle.position.z + radio * Mathf.Sin(Mathf.PI - angle));
      rightCastlePositions[i]=new PositionData(true, new_position);

      //Instantiate(test, new_position, Quaternion.identity);

    }

    lockedPositions = 0;

    EventManager.freePosition += freePosition;
    EventManager.lockPosition += getNextPosition;

  }

  /// Check the next position avaliable:
  /// First check if there is free positions around the tower
  /// if not go to the next pivot
  public Vector3 getNextPosition(Vector3 current_position, Side side) {
    PositionData locked = new PositionData(false, Vector3.down);
    float min_distance = float.MaxValue;
    float aux_distance = float.MaxValue;

    //Check turret
    foreach (PositionData data in (side == Side.Left) ? rightCastlePositions : leftCastlePositions) {
      if (data.avaliable) {
        if ((aux_distance = getDistance(data.position, current_position)) < min_distance) {
          min_distance = aux_distance;
          locked = data;
          //attack = true;
        }
      }//already locked
      else if (data.position == current_position) return current_position;
    }

    //never locked and no avaliabvle position
    if(locked.position == Vector3.down) return getPivotPosition(current_position, side);

    //lock new position
    lockedPositions +=1;
    locked.avaliable = false;
    return locked.position;

  }

  //Calculate the distance between two positions
  float getDistance(Vector3 origin, Vector3 destination) {
    return (destination - origin).magnitude;
  }

  //Get next pivot position
  private Vector3 getPivotPosition(Vector3 position, Side side) {
    int index = 0;
    int length = pivotsRightCastle.Length;

    for (index = 0; index < length; ++index) {
      if (side == Side.Left) {//go right
        if (position == pivotsRightCastle[index].position) {
          index++;
          break;
        }
      } else {//go left
        if (position == pivotsLeftCastle[index].position) {
          index++;
          break;
        }
      }
      
    }

    if (side == Side.Left) {
      return pivotsRightCastle[index % length].position;
    } 
    return pivotsLeftCastle[index % length].position;
  }

  /*public Vector3 lockPosition(Vector3 position, Side side) {
    bool locked = false;
    foreach (PositionData data in
             (position.x < 0) ? leftCastlePositions : rightCastlePositions) {
      if (data.avaliable && getDistance(data.position, position) < 0.5f) {//aprox
        data.avaliable = false;
        locked = true;
        lockedPositions++;
        break;
      }
    }
    if (!locked) {
      if (lockedPositions == attackPositions) return getPivotPosition(position, side);
      Debug.Log("ALREADY LOCKED POSITION");
      Vector3 pos = getNextPosition(position, side);
      Debug.Log("NEW Position = " + pos);
      pos.z *= 1.5f;
      Debug.Log("NEW Position = " + pos);
      return pos;
    }

    return position;
  }*/


  /// free a locked position around the tower
  void freePosition(freePositionMsg msg) {
    if (msg.position == Vector3.down) return;//defautl case (pool), just ignore them

    int length = rightCastlePositions.Length;

    for (int index = 0; index < length; ++index) {
      if (msg.position.x > 0) {//go right
        if (msg.position == rightCastlePositions[index].position) {
          rightCastlePositions[index].avaliable = true;
          lockedPositions--;
          break;
        }
      } else {//go left
        if (msg.position == leftCastlePositions[index].position) {
          leftCastlePositions[index].avaliable = true;
          lockedPositions--;
          break;
        }
      }
    }
  }

}
