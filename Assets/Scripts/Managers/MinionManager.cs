﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class MinionManager : MonoBehaviour {

  public List<TeamSO> avaliableTeams;//teams
  
  public int poolElements = 50;

  private List<GameObject>[,] minionPoolList;// bidimensional array of Lists
                                             // the first index indicate the team
                                             // the second index the type of minion

  /// This function is called when the scene game starts creating a pool of poolElements
  /// for each type of minions in the player team and in the enemy team and
  /// store them in the minionPoolList
  public void createPool() {
    
    List<MinionSO> playerMinions = avaliableTeams[GameInstance.Instance.PlayerTeam].characterList;
    List<MinionSO> enemyMinions = avaliableTeams[GameInstance.Instance.EnemyTeam].characterList;

    int num_playerMinions = playerMinions.Count;
    int num_enemyMinions = enemyMinions.Count;

    //create array
    minionPoolList = new List<GameObject>[2, Mathf.Max(num_playerMinions, num_enemyMinions)];

    /*Player*/
    for (int j = 0; j < playerMinions.Count; ++j) { // to each MinionSO...
      minionPoolList[0,j] = new List<GameObject>(); // ...create a pool List
      for (int i = 0; i < poolElements; ++i){ //create pool of poolElements
        GameObject obj = Instantiate(playerMinions[j].model, GameInstance.Instance.gameObject.transform) as GameObject;
        obj.SetActive(false);
        minionPoolList[0, j].Add(obj);
      }
    }

    /*Enemy*/
    for (int j = 0; j < enemyMinions.Count; ++j) { // to each MinionSO...
      minionPoolList[1, j] = new List<GameObject>(); // ...create a pool List
      for (int i = 0; i < poolElements; ++i) { //create pool of poolElements
        GameObject obj = Instantiate(enemyMinions[j].model, GameInstance.Instance.gameObject.transform) as GameObject;
        obj.SetActive(false);
        minionPoolList[1, j].Add(obj);
      }
    }

  }

  /// Given the team and the type of minion the function spawn one avaliable minion
  /// and tags it in function of the side
  public void spawnMinion(int[] index, Side side) {
    GameObject minion = null;
    Vector3 new_position = getPosition();

    if (new_position != Vector3.zero) {//avoiding errors
      minion = getMinionObject(index);
      if (minion != null) {//if there is a minion avaliable
        minion.transform.position = new_position;
        minion.transform.rotation = Quaternion.identity;

        int team = (index[0] == 0) ? GameInstance.Instance.PlayerTeam : GameInstance.Instance.EnemyTeam;
        Stats stats = getStats(avaliableTeams[team].characterList[index[1]]);
        minion.GetComponent<Minion>().setStats(stats);
        minion.GetComponent<Minion>().team = side;
        if (side == Side.Left) {
          minion.tag = "LeftTeam";
        } else {
          minion.tag = "RightTeam";
        }
        minion.GetComponent<Minion>().getFinalPosition();
        minion.SetActive(true);
      }
    }
  }

  /// Calculate the position where the minion is gonna spawn
  Vector3 getPosition() {
    Vector3 result = Vector3.zero;
    
    if (Input.GetButtonDown("Fire1")) {//player spawn
      Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
      RaycastHit hit;

      if (Physics.Raycast(ray, out hit)) {//check raycast intersection
        if (GameInstance.Instance.PlayerSide == Side.Left && hit.point.x < 0 ||
            GameInstance.Instance.PlayerSide == Side.Right && hit.point.x > 0) {
          result = new Vector3(hit.point.x, -1.0f, hit.point.z);
        }
      }
    } else { //cpu spawn
      if (GameInstance.Instance.PlayerSide == Side.Left){
        result = new Vector3(Random.Range(0.0f, 20.0f), -1.0f, Random.Range(-10.0f, 10.0f));
      } else {
        result = new Vector3(Random.Range(-20.0f, 0.0f), -1.0f, Random.Range(-10.0f, 10.0f));
      }
    }
    return result;
  }

  /// Checks if there is any avaliable minion in the pool.
  /// If there isn't, the function creates one.
  /// If there is any type of problem creating the minion, it will return a null
  GameObject getMinionObject(int[] index){
    //look in the pool list for avaliable objects
    foreach (GameObject go in minionPoolList[index[0], index[1]]) {
      if (!go.activeInHierarchy) {
        go.transform.parent = null;
        return go;
      }
    }
    //if there is not objects avaliable => create one
    int team = (index[0] == 0) ? GameInstance.Instance.PlayerTeam : GameInstance.Instance.EnemyTeam;
    GameObject new_minion = Instantiate(avaliableTeams[team].characterList[index[1]].model,
                                        GameInstance.Instance.gameObject.transform) as GameObject;
    if (new_minion != null) {
      new_minion.SetActive(false);
      minionPoolList[index[0], index[1]].Add(new_minion);
      new_minion.transform.parent = null;
    }
    return new_minion;
  }

  Stats getStats(MinionSO minion) {
    Stats new_stats = new Stats();

    new_stats.name = minion.minionName;
    new_stats.maxHealth = minion.maxHealth;
    new_stats.attack = minion.attack;
    new_stats.attackSpeed = minion.attackSpeed;
    new_stats.range = minion.range;
    new_stats.detectionRange = minion.detectionRange;
    new_stats.speed = minion.speed;

    return new_stats;
  }
}
