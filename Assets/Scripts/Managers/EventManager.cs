﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public struct attackMsg {
  public GameObject attacker;
  public GameObject killed;
}

public struct freePositionMsg {
  public Vector3 position;

  public freePositionMsg(Vector3 new_position) {//Ctor
    this.position = new_position;
  }
}

public static class EventManager {

  //Delegates
  public delegate void EventHandler(attackMsg data);
  public delegate void orderHandler();
  public delegate void freePositionHandler(freePositionMsg data);
  public delegate Vector3 lockPositionHandler(Vector3 position, Side side);

  //Handlers
  public static event EventHandler EventTriggered;
  public static event orderHandler orderEvent;
  public static event freePositionHandler freePosition;
  public static event lockPositionHandler lockPosition;


  public static void OnEventTriggered(attackMsg data) {
    if (EventTriggered != null) {//not empty
      EventTriggered(data);
    }
  }

  public static void OnorderEvent() {
    if (orderEvent != null){//not empty
      orderEvent();
    }
  }

  public static void OnPositionFree(freePositionMsg data) {
    if (freePosition != null) {//not empty
      freePosition(data);
    }

  }

  public static Vector3 OnLockPosition(Vector3 position, Side side) {
    if (lockPosition != null) {//not empty
      return lockPosition(position, side);
    }
    return Vector3.zero;
  }

}
