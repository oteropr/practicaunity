﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIManager : MonoBehaviour {

  public Canvas myCanvas;

  private GameMode controller = null;
  private Transform logPannel;
  private Text timerText;
  private string[] logMsg;


  // Use this for initialization
  void Start () {

    //reference to GameMode
    controller = this.GetComponent<GameMode>();

    //reference to components
    logPannel = myCanvas.transform.FindChild("LogPannel").GetComponent<Transform>();
    timerText = myCanvas.transform.FindChild("Timer").GetComponent<Text>();

    timerText.text = "00:00";
    logMsg = new string[6];
    for (int i = 0; i < logMsg.Length; ++i) {
      logMsg[i] = "";
    }
    
  }

  // Update is called once per frame
  void Update () {
    //Update timer
    float timer = controller.Timer;
    timerText.text = ((int)(timer / 60.0f)).ToString("00") + ":" + ((int)(timer % 60.0f)).ToString("00");

  }

  /// Show end game Messages
  public void setEndGameMsg(string text) {
    Text alert = myCanvas.transform.FindChild("EndGameText").GetComponent<Text>();
    alert.gameObject.SetActive(true);
    alert.text = text;
  }

  /// Show attack messages log
  public void addLogMsg(string text) {
    //update string array
    for (int i = logMsg.Length - 1; i >0 ; --i) {
      logMsg[i] = logMsg[i-1];
    }
    logMsg[0] = text;

    //olderLogText = 5 texts
    string olders = "";
    for (int i = 1; i < logMsg.Length; ++i) {
      olders += logMsg[i] + "\n";
    }
    logPannel.transform.FindChild("olderLogTexts").GetComponent<Text>().text = olders;
    //update last msg
    logPannel.transform.FindChild("lastLogText").GetComponent<Text>().text = logMsg[0];
  }

  /// update type of unit selected
  public void setUnitType(int value) {
    controller.UnitTypeSelected = value;
  }
}
