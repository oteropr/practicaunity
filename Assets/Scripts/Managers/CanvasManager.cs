﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CanvasManager : MonoBehaviour {

  public Camera myCamera;

  private Vector3 cameraAngleVector;

  // Use this for initialization
  void Start() {
    cameraAngleVector =  myCamera.transform.rotation.eulerAngles;

  }

  // Update is called once per frame
  void Update() {
    rotateCamera();
  }

  public void rotateCamera() {
    if (!isCameraInPosition()) {
      Quaternion finalRotation = Quaternion.Euler(cameraAngleVector);
      myCamera.transform.rotation = Quaternion.Slerp(myCamera.transform.rotation,
                                                     finalRotation,
                                                     Time.deltaTime * 5);
    }

  }

  public void set_GameMode(int value) {
    GameInstance.Instance.GameMode = (gameMode)value;
    cameraAngleVector.y = 90.0f;
  }

  public void set_Side(int value) {
    GameInstance.Instance.PlayerSide = (Side)value;
    cameraAngleVector.y = 180.0f;
  }

  public void set_PlayerTeam(int value) {
    if (value == -1) {//Random
      value = Random.Range(0, 2);
    }
    GameInstance.Instance.PlayerTeam = value;
    cameraAngleVector.y = 270.0f;
  }

  public void set_EnemyTeam(int value){
    if (value == -1){//Random
      value = Random.Range(0, 2);
    }
    GameInstance.Instance.EnemyTeam = value;
    GameInstance.Instance.loadGameScene();
  }

  private bool isCameraInPosition() {
    return (cameraAngleVector.x == myCamera.transform.rotation.x &&
            cameraAngleVector.y == myCamera.transform.rotation.y &&
            cameraAngleVector.z == myCamera.transform.rotation.z);
  }





}
